"""
    Class implementation for metrics publisher
"""

__all__ = ["MetricsPublisher"]
__author__ = ["Shlok Chaudhari"]


import time
import threading

from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.model_monitoring.message_queue import MessageQueueFactory
from xpresso.ai.core.commons.utils.generic_utils import generate_uid
from xpresso.ai.core.commons.exceptions.xpr_exceptions import InvalidModelMonitoringData

from xpresso.ai.core.model_monitoring.metrics_publisher.request_generator import \
    RequestGenerator
from xpresso.ai.core.model_monitoring.metrics_publisher.response_generator import \
    ResponseGenerator
from xpresso.ai.core.model_monitoring.metrics_publisher.operation_metric_generator import \
    OperationMetricsGenerator


class MetricsPublisher:
    """
        This class is used to publish all metrics
        required for model monitoring on a message
        queue
    """

    _logger = XprLogger()
    _stop_thread = False

    def __init__(self, model_id: str, interval: int = 10):
        self._interval = interval
        self._validate()
        self._model_id = model_id
        self._run_operation_metrics_thread()

    def stop_thread(self) -> bool:
        """
            Method that sets a flag to
            kill the background thread
        Returns:
            bool: True if thread is dead
        """
        self._stop_thread = True
        self._thread.join()
        return self._stop_thread

    def is_operation_thread_alive(self) -> bool:
        """
            Method to check if the daemon
            thread for sending system
            operational metrics is alive
        Returns:
            bool: True if alive else False
        """
        return self._thread.is_alive()

    def _run_operation_metrics_thread(self):
        """
            Run daemon thread to publish
            resource usage metrics to queue
        """
        self._logger.info("Running daemon thread")
        self._thread = self._init_thread()
        self._thread.daemon = True
        self._thread.start()

    def _init_thread(self) -> threading.Thread:
        """
            Initialize thread with method and
            required arguments
        Returns:
            thread instance (threading.Thread)
        """
        self._logger.info(f"Initializing thread")
        return threading.Thread(target=self._send_metrics, args=())

    def _send_metrics(self):
        """
            Daemon process where the metrics will be published
            in pre-defined time interval
        """
        if threading.current_thread().name == 'MainThread':
            msg = "This method is not callable from MainThread"
            raise PermissionError(msg)

        self._logger.info("Sending resource usage metrics.\n"
                          f"Sleep interval: {self._interval} secs")
        metrics_generator = OperationMetricsGenerator(self._model_id)
        message_queue = MessageQueueFactory().create_manager()
        while True:
            if self._stop_thread:
                break
            resource_usage_metrics = metrics_generator.get_metrics()
            message_queue.publish(msg=resource_usage_metrics)
            time.sleep(self._interval)

    def _validate(self, data=None):
        """
            Method to validate input to MetricsPublisher
        Args:
            data(dict): request/response data
        Raises:
            InvalidModelMonitoringData: if model_id and interval do not
            conform to standard input
        """
        self._logger.info("Validating input to MetricsPublisher")
        if not isinstance(self._interval, int):
            raise InvalidModelMonitoringData("Interval is not a num")
        if not data:
            return
        if not isinstance(data, dict):
            raise InvalidModelMonitoringData("Req/Resp data is not a dict")

    def on_new_request(self, request_data: dict):
        """
            Send data for monitoring new request
        Args:
            request_data(dict)
        """
        self._logger.info("Sending request data for monitoring")
        self._validate(request_data)
        request_id = request_data.pop("model_request_id")
        metrics_generator = RequestGenerator(self._model_id, request_id, request_data)
        message_queue = MessageQueueFactory().create_manager()
        message_queue.publish(msg=metrics_generator.get_metrics())

    def on_response_ready(self, response_data: dict):
        """
            Send data for monitoring response
        Args:
            response_data(dict)
        """
        self._logger.info("Sending response data for monitoring")
        self._validate(response_data)
        request_id = response_data.pop("model_request_id")
        metrics_generator = ResponseGenerator(self._model_id, request_id, response_data)
        message_queue = MessageQueueFactory().create_manager()
        message_queue.publish(msg=metrics_generator.get_metrics())
