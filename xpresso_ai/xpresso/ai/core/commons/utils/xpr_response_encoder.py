import datetime
from flask.json import JSONEncoder


from xpresso.ai.core.commons.utils.datetime_utils import DatetimeUtils


class XprResponseEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return DatetimeUtils.get_formatted_time(obj)
        return JSONEncoder.default(self, obj)
