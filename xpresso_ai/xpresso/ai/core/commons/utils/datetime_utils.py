from datetime import datetime

from dateutil import parser
from xpresso.ai.core.commons.utils.constants import TIMEFORMAT, DEFAULT_DATE


class DatetimeUtils:
    # python epoch as a datetime object
    first_epoch = datetime.utcfromtimestamp(0)

    @staticmethod
    def get_current_time():
        """Returns current timestamp in utc format"""
        return datetime.utcnow()

    @staticmethod
    def get_current_formatted_time(target_format=TIMEFORMAT) -> str:
        """
        Returns current time in string format
        """
        time = DatetimeUtils.get_current_time()
        return DatetimeUtils.get_formatted_time(time, target_format)

    @staticmethod
    def get_formatted_time(time: datetime, target_format=TIMEFORMAT) -> str:
        """Return standard format time in string
        Args:
            time(datetime): Datetime time object to be converted
            target_format(str): Target format to be converted to.
                default: xpresso time format i.e. %Y-%m-%d %H:%M:%S
        Returns:
            Returns converted datetime time to string.
        """
        if isinstance(time, str):
            time = parser.parse(time)
        formatted_time = time.strftime(target_format)
        return formatted_time

    @staticmethod
    def get_default_datetime():
        """Returns a default date for missing date fields"""
        time = datetime.strptime(DEFAULT_DATE, TIMEFORMAT)
        return time

    @staticmethod
    def convert_str_time_to_datetime(time_in_str: str) -> datetime:
        """Returns datetime object created from provided str"""
        try:
            time_in_datetime = parser.parse(time_in_str)
            return time_in_datetime
        except (parser.ParserError, TypeError):
            return DatetimeUtils.get_default_datetime()

    @staticmethod
    def check_if_date_is_in_30_days(query_date: datetime) -> bool:
        """
        checks if the input date is within 30 days period to current time

        Args:
            query_date: date in query, in  datetime format
        Returns:
            True: if query_date is within 30 days to current time
            False: if query_date is older than 30 days
        """
        time_delay = datetime.utcnow() - query_date
        return time_delay.days <= 30

    @staticmethod
    def get_datetime_object_from_seconds_count(
            seconds_since_jan1_1970: int) -> datetime:
        """
        In most windows & UNIx systems epoch is Jan 1 1970 00:00
        Python uses the above epoch for time measurements
        This method takes the total seconds since the epoch and returns a
        datetime object for that time

        Args:
            seconds_since_jan1_1970: total seconds since the epoch Jan 1 1970
            00:00
        """
        utc_datetime = datetime.utcfromtimestamp(seconds_since_jan1_1970)
        return utc_datetime

    @staticmethod
    def get_formatted_time_from_seconds_count(
            seconds_since_jan1_1970: int) -> str:
        """
        Fetch the time in string format given the total seconds spanned
        since the first epoch on Jan 1 1970 00:00

        Args:
            seconds_since_jan1_1970: total seconds since the epoch Jan 1 1970
            00:00
        """
        datetime_object = DatetimeUtils.get_datetime_object_from_seconds_count(
            seconds_since_jan1_1970)
        formatted_time = DatetimeUtils.get_formatted_time(datetime_object)
        return formatted_time
