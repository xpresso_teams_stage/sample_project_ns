import os
import re
import shutil
from os import path as path_library, walk as walk_dir_library

import pandas as pd
from xpresso.ai.core.commons.utils.constants import FILE_TIMEFORMAT
from xpresso.ai.core.commons.utils.datetime_utils import DatetimeUtils


def to_excel(output_path, filename, sheets=dict()):
    """Helper function to output datafram to excel
    Args:
        output_path('str"): path where the excel file is to be stored
        filename('str'): filename of the excel file
        sheets(:dict): dictionary with sheet name as key and dataframes as
        values
    """
    if not os.path.exists(output_path):
        os.makedirs(output_path)
    writer = pd.ExcelWriter(os.path.join(output_path, filename),
                            engine='xlsxwriter')
    for sheet in sheets:
        sheets[sheet].to_excel(writer, sheet_name=sheet.upper(), index=False,
                               header=False)
    writer.save()
    # to provide output at user end
    print("File saved to {}".format(os.path.join(output_path, filename)))


def get_file_from_path_string(path, file_name):
    """
    Get file name from path if exists
    Args:
        path(str): path to be split
        file_name(str): file name if explicitly provided
    Returns:
         Processed output path and filename
    """
    output_file_name = os.path.basename(path)
    output_path = os.path.dirname(path)
    temp_filename, temp_ext = os.path.splitext(output_file_name)
    if not temp_ext:
        output_path = path
    if file_name:
        output_file_name = file_name
    output_path = os.path.abspath(os.path.join(os.getcwd(), output_path))
    output_file_name = append_timestamp(output_file_name)
    return output_path, output_file_name


def append_timestamp(file_name):
    """ Appends current timestamp to file name
    Args:
        file_name(str): file name to which timestamp is to be appended
    Returns:
        File name with current timestamp"""
    temp_filename, temp_ext = os.path.splitext(file_name)
    timestamp = DatetimeUtils.get_current_formatted_time(FILE_TIMEFORMAT)
    temp_filename = "{}_{}".format(temp_filename, timestamp)
    return "{}{}".format(temp_filename, temp_ext)


def set_extension(file_name, extension):
    """
    Set extension for filename
    Args:
        file_name(str): filename whose extension to be set
        extension(str): extension of the filename to be set
    Returns:
        Filename with correct extension
    """
    temp_filename, temp_ext = os.path.splitext(file_name)
    if not extension.startswith('.'):
        extension = ".{}".format(extension)
    if temp_ext == extension:
        return file_name
    return "{}{}".format(temp_filename, extension)


def get_json_from_dir(base_path):
    """
    fetches the path to json file inside the base_path dir

    :param base_path:
        path to the base directory
    :return:
        path to the json file
    """
    exp = ".*_xpr_dataset__.*\.json$"
    for current_dir_path, subdir_list, files in walk_dir_library(base_path):
        # sort wrt timestamp
        files = sorted(files, reverse=True)
        for file in files:
            file_path = path_library.join(current_dir_path, file)
            if re.search(exp, file_path):
                return file_path
    return None


def get_csv_from_dir(base_path):
    """
    fetches the path to csv file inside the base_path dir

    :param base_path:
        path to the base directory
    :return:
        path to the csv file
    """
    exp = ".*_xpr_dataset__.*\.csv$"
    for current_dir_path, subdir_list, files in walk_dir_library(base_path):
        # sort wrt timestamp
        files = sorted(files, reverse=True)
        for file in files:
            file_path = path_library.join(current_dir_path, file)
            if re.search(exp, file_path):
                return file_path
    return None


def check_file(file_name, output_path=None):
    """Checks if the file name exists in the output path directory
    Args:
        file_name (str): file name to be searched
        output_path (str): path of the directory in
         which file name is to be searched
    Returns:
        The return value. True if file name exists, otherwise False
        """
    if output_path is None:
        return False
    try:
        files = os.listdir(output_path)
    except FileNotFoundError:
        return False
    file_name, extention = os.path.splitext(file_name)
    exp = "^" + file_name + "[^a-zA-Z0-9].*" + extention + "$"
    for file in files:
        if re.search(exp, file):
            return True
    return False


def remove_dir(path=None):
    """Removes the directory/file
    Args:
        path (str): The path to the directory/file
         to be removed"""
    if path is None:
        return False
    if os.path.exists(path):
        shutil.rmtree(path)


def convert_bytes_to_gb(num):
    """
    This function will convert bytes to GB
    """
    return round(num / (1024.0 * 1024.0 * 1024), 4)


def get_dataframe_size(data_frame):
    """
    This function returns size of DataFrame in MB
    Args:
        data_frame(DataFrame): DataFrame of which size to be calculated
    """
    return convert_bytes_to_gb(data_frame.memory_usage(deep=True).sum())
